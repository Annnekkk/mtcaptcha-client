"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUA = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
async function getUA() {
    try {
        const response = await (0, node_fetch_1.default)('https://fix.annnek.online/ranua?datatype=json');
        const data = await response.json();
        if (!data.data) {
            throw new Error('API response does not contain a User-Agent string.');
        }
        return data.data;
    }
    catch (error) {
        console.error('Error fetching User-Agent:', error);
        throw error;
    }
}
exports.getUA = getUA;
