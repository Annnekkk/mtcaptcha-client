"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MtCaptchaClient = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
const const_1 = require("./const");
const crypto_1 = require("./crypto");
const utils_1 = require("./utils");
class MtCaptchaClient {
    /**
     * Create a new MtCaptchaClient
     * @param {string} siteUrl - The domain of the site where the mtcaptcha you want to solve is located
     * @example "www.mtcaptcha.com"
     * @param {string} siteKey - The site key for the customer/site where the mtcaptcha is located, can be found in the network tab or inspect site & look for "siteKey"
     * @example "MTPublic-tqNCRE0GS"
     */
    constructor(siteUrl, siteKey) {
        this.siteUrl = siteUrl;
        this.siteKey = siteKey;
    }
    /**
     * Create a new captcha challenge
     * @returns {Promise<Challenge>} A captcha challenge
     */
    async createChallenge() {
        const { sessionId, challenge } = await this.getChallenge();
        // timeout here seems to be needed in order to make sure challenge is ready
        await new Promise((r) => setTimeout(r, 1000));
        const image = await this.getImage(sessionId, challenge);
        const audio = await this.getAudio(sessionId, challenge);
        return {
            sessionId,
            image,
            audio,
            ...challenge,
        };
    }
    /**
     * Try solving a captcha challenge, returns a verifiedToken if successful
     * @param {string} challenge - The captcha challenge to solve
     * @param {string} solution - The suggested solution to the captcha challenge
     */
    async verifyChallenge(challenge, solution) {
        const params = new URLSearchParams({
            ct: challenge.ct,
            sk: this.siteKey,
            st: solution,
            lf: "0",
            bd: this.siteUrl,
            rt: new Date().getTime().toString(),
            tsh: const_1.TSH,
            fa: challenge.hasFoldChlg
                ? (0, crypto_1.solveChallenge)(challenge.foldChlg.fseed, challenge.foldChlg.fslots, challenge.foldChlg.fdepth)
                : "$",
            qh: "$",
            act: "$",
            ss: challenge.sessionId,
            tl: "10",
            lg: "sv",
            tp: "s",
            kt: (0, crypto_1.getKeesString)(challenge.foldChlg.fseed),
            fs: challenge.foldChlg.fseed,
        });
        const solutionResponse = await (0, node_fetch_1.default)(`${const_1.BASE_URL}/api/solvechallenge.json?${params}`, {
            headers: {
                ...(0, utils_1.generateHeaders)(this.siteUrl, this.siteKey),
                Referer: this.siteUrl,
            },
        });
        const { result } = (await solutionResponse.json());
        const { isVerified, verifiedToken } = result.verifyResult;
        return isVerified
            ? { verifiedToken: verifiedToken.vt, isVerified }
            : { isVerified: false };
    }
    async getChallenge() {
        const sessionId = (0, utils_1.generateSessionId)();
        const params = new URLSearchParams({
            sk: this.siteKey,
            bd: this.siteUrl,
            rt: new Date().getTime().toString(),
            tsh: const_1.TSH,
            act: "$",
            ss: sessionId,
            lf: "0",
            tl: "10",
            lg: "sv",
            tp: "s",
        });
        const challengeResponse = await (0, node_fetch_1.default)(`${const_1.BASE_URL}/api/getchallenge.json?${params}`, {
            headers: (0, utils_1.generateHeaders)(this.siteUrl, this.siteKey),
        });
        const { result } = (await challengeResponse.json());
        return {
            sessionId,
            challenge: result.challenge,
        };
    }
    async getImage(sessionId, mtChallenge) {
        const params = new URLSearchParams({
            sk: this.siteKey,
            ct: mtChallenge.ct,
            fa: mtChallenge.hasFoldChlg
                ? (0, crypto_1.solveChallenge)(mtChallenge.foldChlg.fseed, mtChallenge.foldChlg.fslots, mtChallenge.foldChlg.fdepth)
                : "$",
            ss: sessionId,
        });
        const imageResponse = await (0, node_fetch_1.default)(`${const_1.BASE_URL}/api/getimage.json?${params}`, {
            headers: {
                ...(0, utils_1.generateHeaders)(this.siteUrl, this.siteKey),
                cookie: `mtv1Pulse=${(0, crypto_1.getPulse)(this.siteUrl)}; mtv1ConfSum={v:01|wdsz:std|thm:highcontrast|lan:sv|chlg:std|clan:1|cstyl:1|afv:0|afot:0|}; jsV=2024-03-24.15.25.36`,
            },
        });
        const { result } = (await imageResponse.json());
        return result.img.image64;
    }
    async getAudio(sessionId, mtChallenge) {
        const params = new URLSearchParams({
            sk: this.siteKey,
            ct: mtChallenge.ct,
            fa: mtChallenge.hasFoldChlg
                ? (0, crypto_1.solveChallenge)(mtChallenge.foldChlg.fseed, mtChallenge.foldChlg.fslots, mtChallenge.foldChlg.fdepth)
                : "$",
            ss: sessionId,
        });
        const audioResponse = await (0, node_fetch_1.default)(`${const_1.BASE_URL}/api/getaudio.json?${params}`, {
            headers: {
                ...(0, utils_1.generateHeaders)(this.siteUrl, this.siteKey),
                cookie: `mtv1Pulse=${(0, crypto_1.getPulse)(this.siteUrl)}; mtv1ConfSum={v:01|wdsz:std|thm:highcontrast|lan:sv|chlg:std|clan:1|cstyl:1|afv:0|afot:0|}; jsV=2024-03-24.15.25.36`,
            },
        });
        const { result } = (await audioResponse.json());
        return result.aud.audio64;
    }
}
exports.MtCaptchaClient = MtCaptchaClient;
