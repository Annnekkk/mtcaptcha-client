"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TSH = exports.USER_AGENT = exports.BASE_URL = void 0;
exports.BASE_URL = "https://service.mtcaptcha.com/mtcv1";
exports.USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 14_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.6 Safari/605.1.15";
exports.TSH = "TH[6e09ce340b08173427d0b30273455a6c]";
