export declare const getPulse: (siteUrl: string) => string;
export declare function getKeesString(fseed: string): string;
export declare function solveChallenge(fseed: string, fslots: number, fdepth: number): string;
