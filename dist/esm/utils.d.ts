export declare const generateSessionId: () => string;
export declare const generateHeaders: (siteUrl: string, siteKey: string) => Promise<{
    "User-Agent": string;
    referer: string;
    origin: string;
}>;
