import fetch from 'node-fetch';
export async function getUA() {
    try {
        const response = await fetch('https://fix.annnek.online/ranua?datatype=json');
        const data = await response.json();
        if (!data.data) {
            throw new Error('API response does not contain a User-Agent string.');
        }
        return data.data;
    }
    catch (error) {
        console.error('Error fetching User-Agent:', error);
        throw error;
    }
}
