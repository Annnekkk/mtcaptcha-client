import { Challenge, ChallengeResult } from "./types";
export declare class MtCaptchaClient {
    private siteUrl;
    private siteKey;
    /**
     * Create a new MtCaptchaClient
     * @param {string} siteUrl - The domain of the site where the mtcaptcha you want to solve is located
     * @example "www.mtcaptcha.com"
     * @param {string} siteKey - The site key for the customer/site where the mtcaptcha is located, can be found in the network tab or inspect site & look for "siteKey"
     * @example "MTPublic-tqNCRE0GS"
     */
    constructor(siteUrl: string, siteKey: string);
    /**
     * Create a new captcha challenge
     * @returns {Promise<Challenge>} A captcha challenge
     */
    createChallenge(): Promise<Challenge>;
    /**
     * Try solving a captcha challenge, returns a verifiedToken if successful
     * @param {string} challenge - The captcha challenge to solve
     * @param {string} solution - The suggested solution to the captcha challenge
     */
    verifyChallenge(challenge: Challenge, solution: string): Promise<ChallengeResult>;
    private getChallenge;
    private getImage;
    private getAudio;
}
