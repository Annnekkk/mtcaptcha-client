import fetch from 'node-fetch';

interface RandomUserAgentResponse {
  data: string;
}

export async function getUA(): Promise<string> {
  try {
    const response = await fetch('https://fix.annnek.online/ranua?datatype=json');
    const data: RandomUserAgentResponse = await response.json();

    if (!data.data) {
      throw new Error('API response does not contain a User-Agent string.');
    }
    
    return data.data;
  } catch (error) {
    console.error('Error fetching User-Agent:', error);
    throw error;
  }
}
